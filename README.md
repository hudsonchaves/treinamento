### **TREINAMENTO **

Nesta wiki você encontrará informações sobre as tecnologias que utilizo em projetos de pesquisa bem como algumas dicas importantes para que sua pesquisa possa ser **totalmente reproduzível e escalável**:

* Reproduzível de forma que qualquer pessoa com acesso ao repositório de códigos da pesquisa pode clonar o projeto e reproduzir as análises seja em um computador prório ou servidor, independente do sistema operacional adotado (Linux e Windows).
* Escalável no sentido que sua pesquisa pode ser executada em servidores na nuvem (por enquanto ensino como fazer uso da [Google Cloud](https://cloud.google.com/?hl=pt-br)) caso queira maior capacidade de processamento de dados.

Neste [link](https://bitbucket.org/hudsonchaves/treinamento/wiki/Home) você acessará a página inicial da wiki. Para melhor entendimento, recomendo a leitura seguindo o sumário. Caso você já tenha familiaridade com algo fique à vontade para alterar a sequência da leitura conforme seu interesse. 

Sugestões de melhoria e críticas são bem vindas!!! Adicione uma issue neste [link](https://bitbucket.org/hudsonchaves/treinamento/issues?status=new&status=open) que farei o possível para sanar sua dúvida o quanto antes!!

### **O QUE VOCÊ APRENDERÁ NESSA WIKI**

Além de detalhes sobre as tecnologias, em linhas gerais ao final da leitura dessa wiki você saberá:

* Como montar um ambiente de trabalho utilizando Vagrant, Virtual Box e Ansible tanto localmente (Linux e Windows) quanto em servidores;
* Como usar imagens Docker para montar uma arquitetura de processamento e análise de dados;
* Como fazer uso do Docker para distribuir sua pesquisa.