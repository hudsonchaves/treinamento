# Arquivo que declara o cabeçalho dos arquivos

colnames.dadosficticios = c("Car", "aquisition_data", "customer_satisfaction")

colnames.mtcars = c("Car", "mpg", "cyl", "disp", "hp", "drat", "wt", "qsec", "vs", "am", "gear", "carb")

colname.interesse = list(colnames.dadosficticios, colnames.mtcars)
