#################################################################
#####          ANÁLISE EXPLORATÓRIA UNIVARIADA              #####
#################################################################

# Esse código tem como objetivo automatizar o processo de análise exploratória univariada. 
# Como resultado, o código gera base de dados com as principais métricas utilizadas na 
# análise exploratória univariada. Assim, conseguimos separar a geração dos resultados
# da análise exploratória univariada da criação dos documentos que disponibilizam para os
# clientes os resultados da análise. Além disso, todo o processo demanda menor manutenção e 
# uso de recursos computacionais, uma vez que a execução só precisa ser feita uma vez para 
# que os resultados da análise sejam criados. 

# Outputs:
# em /api/exchange/ae/univariada/dados/outputs serão disponibilizados três arquivos:
# @dimensões: extrai dos arquivos as quantidades de colunas em cada;
# @numericas: extrai
# @categoricas: extrai
# @datas: extrai

##########################################
#########       PACOTES           ########
##########################################

suppressMessages(require(data.table)) # função fread
suppressMessages(require(plyr))
suppressMessages(require(dplyr))
suppressMessages(require(tidyr))
suppressMessages(require(lubridate)) # utilizado para cálculos datas
suppressMessages(require(readr))
suppressMessages(require(DT))
suppressMessages(require(datasets))
suppressMessages(require(bit64))

options(lubridate.fasttime = TRUE)

##########################################
####            FUNÇÕES              #####
##########################################

# @dat: dados
# @oldnames: nome atual das colunas
# @newnames: novo nome para as colunas
# Esta função é usada para renomear as colunas dos data frames após o join entre
# as tabelas de interesse (consultoria e arquivos do poc)

rename.cols = function(dat, oldnames, newnames) {
  datnames = colnames(dat)
  datnames[which(datnames %in% oldnames)] = newnames
  colnames(dat) = datnames
  tbl_df(dat)
}

##########################################
####     PATH PARA ARQUIVOS          #####
##########################################

#####
### Todos os arquivos em /api/arquivos
#####

files.path = as.list(list.files(path = "C:/Users/Natalia/integracao_novos_membros-wiki/Dados/Exemplo_script_univariada", 
                                pattern = "\\.csv$", full.names = TRUE)) # nome completo do local
files.nome = as.list(list.files(path = "C:/Users/Natalia/integracao_novos_membros-wiki/Dados/Exemplo_script_univariada", 
                                pattern = "\\.csv$", full.names = FALSE)) # nome do arquivo apenas

##########################################
####     NOME DAS VARIÁVEIS          #####
##########################################

#####
### Todos os arquivos em /api/arquivos
#####

# É importante manter a mesma sequência da apresentada nos arquivos listados
source("C:/Users/Natalia/integracao_novos_membros-wiki/code/R/Script_Univariada_Exemplo/AE_Automatizada_Cabecalhos.R")

##########################################
####     TIPO DAS VARIÁVEIS          #####
##########################################

#####
### Todos os arquivos em /api/arquivos
#####

# É importante manter a mesma sequência da apresentada nos arquivos listados em "files.path"
source("C:/Users/Natalia/integracao_novos_membros-wiki/code/R/Script_Univariada_Exemplo/AE_Automatizada_Datas.R")
source("C:/Users/Natalia/integracao_novos_membros-wiki/code/R/Script_Univariada_Exemplo/AE_Automatizada_Fator.R")

##########################################
####      DADOS EM API/ARQUIVOS      #####
##########################################

#####
##  DESCOBRIR A QUANTIDADE DE COLUNAS EM CADA ARQUIVO
#####

# Antes de iniciar a análise é interessante verificar se a quantidade de variáveis presentes 
# em cada arquivo confere com a disponibilizada pelo cliente. Assim, fazemos a leitura automática
# de todos os arquivos e obtemos a quantidade de colunas presente em cada. Para tanto, fazemos a 
# leitura de apenas 10 linhas de cada arquivo.

initial.all.csvs = lapply(1:length(files.path), function(i, files.path)
  fread(files.path[[i]], header = FALSE, sep = ";", skip = 1, nrows = 10, encoding = 'Latin-1', data.table = FALSE)
  , files.path = files.path)


for (i in 1:length(initial.all.csvs)) {
  if (ncol(initial.all.csvs[[i]]) > 1) {
    initial.all.csvs[[i]] = initial.all.csvs[[i]] 
  } else {
    initial.all.csvs[[i]] = fread(files.path[[i]], header = FALSE, sep = "auto", nrows = 10, skip = 1, encoding = 'Latin-1', data.table = FALSE)
  }
}

dim.csvs = lapply(1:length(initial.all.csvs), function(i) {
  Linhas = dim(initial.all.csvs[[i]])[1]
  Colunas = dim(initial.all.csvs[[i]])[2]
  data.frame(Linhas, Colunas, files.nome[[i]])
})

dim.names = lapply(1:length(colname.interesse), function(i){
  length(colname.interesse[[i]])
})

dim.csvs = ldply(dim.csvs)
dim.names = ldply(dim.names)
dimensoes = cbind(dim.csvs, dim.names)

#####
##  LEITURA DOS ARQUIVOS COMPLETOS
#####

# Nesta etapa você pode escolher dentre os arquivos mapeados quais serão carregados no R
# ou se quer fazer todos de um só vez. Para tanto, selecione os arquivos conforme abaixo. 
# Utilizamos a função fread que tem ótima performance para leitura de grande quantidade de
# dados. Além disso, você deve escolher o cabeçalho específico para cada arquivo e também 
# definir as variáveis que você quer especificar o seu tipo (datas e fatores)

interesse.path =  NULL 
interesse.name =  NULL

colname.interesse = colname.interesse
dates.interesse = dates.interesse
fator.interesse = fator.interesse

# Leitura sem considerar nomes e diferenças entre separador (; e ,)
if (!is.null(interesse.path)) {
  all.csvs = lapply(1:length(interesse.path), function(i, files.path) 
    fread(interesse.path[[i]], header = FALSE, sep = ";", skip = 1, nrows = 28000,  data.table = FALSE, encoding = 'Latin-1'),
    files.path = interesse.path)
} else {
  all.csvs = lapply(1:length(files.path), function(i, files.path) 
    fread(files.path[[i]], header = FALSE, sep = ";", skip = 1, nrows = 28000, data.table = FALSE, encoding = 'Latin-1')
    , files.path = files.path)
}

# Atualizando arquivos que estão com "," como separador para fazer a leitura correta
if (!is.null(interesse.path)){
  for (i in 1:length(all.csvs)) {
    if (ncol(all.csvs[[i]]) > 1) {
      all.csvs[[i]] = all.csvs[[i]]
    } else {
      all.csvs[[i]] = fread(interesse.path[[i]], header = FALSE, sep = "auto", skip = 1, nrows = 28000, encoding = 'Latin-1')
    }
  }
} else{
  for (i in 1:length(all.csvs)) {
    if (ncol(all.csvs[[i]]) >1) {
      all.csvs[[i]] = all.csvs[[i]]
    } else {
      all.csvs[[i]] = fread(files.path[[i]], header = FALSE, sep = "auto", skip = 1, nrows = 28000, encoding = 'Latin-1')
    }
  }
}

# Declarando os nomes das variáveis dentro de cada arquivo
if (!is.null(interesse.path)) {
  for (i in 1:length(all.csvs)) {
    setnames(all.csvs[[i]], colname.interesse[[i]])
  }
} else {
  for (i in 1:length(all.csvs)) {
    setnames(all.csvs[[i]], colname.interesse[[i]])
  }
}

# renomear a lista com os nomes dos arquivos disponibilizados
if (!is.null(interesse.path)) {
  names(all.csvs) = interesse.name
} else {
  names(all.csvs) = files.nome
}

#####
##  TRANSFORMANDO COLUNAS EM DATA
#####

for (i in 1:length(all.csvs)) {
  if (!is.null(interesse.path)) {
    colunas = dates.interesse[[i]]
    if (length(colunas) == 0) {
      next
    } else {
      for (j in 1:length(colunas)) {
        all.csvs[[i]][,dates.interesse[[i]][j]] = as.Date(fast_strptime(as.character(all.csvs[[i]][,dates.interesse[[i]][j]]), format = "%m/%d/%Y"))  
      }       
    }
  } else {
    colunas = dates[[i]]
    if (length(colunas) == 0) {
      next
    } else {
      for (j in 1:length(colunas)) { 
        all.csvs[[i]][,dates[[i]][j]] = as.Date(fast_strptime(as.character(all.csvs[[i]][,dates[[i]][j]]), format = "%m/%d/%Y")) 
      }
    }
  }
}


#####
##  TRANSFORMANDO COLUNAS EM FATOR
#####

for (i in 1:length(all.csvs)) {
  if (!is.null(interesse.path)) {
    colunas = fator.interesse[[i]]
    if (length(colunas) == 0) {
      next
    } else {
      for (j in 1:length(colunas)) {
        all.csvs[[i]][,fator.interesse[[i]][j]] = formatC(all.csvs[[i]][,fator.interesse[[i]][j]])  
      }       
    }
  } else {
    colunas = fator[[i]]
    if (length(colunas) == 0) {
      next
    } else {
      for (j in 1:length(colunas)) { 
        all.csvs[[i]][,fator[[i]][j]] = formatC(all.csvs[[i]][,fator[[i]][j]]) 
      }
    }
  }
}

##############################################
########       TIPO DE VARIÁVEL      #########
##############################################

#####
##  NUMÉRICAS
#####

# Faz um subset em cada data frame da lista separando apenas as variáveis que são numéricas
all.continuas = lapply(1:length(all.csvs), function(i)
  tryCatch(select(all.csvs[[i]], which(sapply(all.csvs[[i]], is.numeric))),  error = function(e) NULL))

# Faz os cálculos de interesse para as variáveis numéricas
result.numericas = lapply(1:length(all.continuas), function(i)
  tryCatch(all.continuas[[i]] %>%
             select(everything()) %>%
             gather('var', 'val') %>%
             group_by(var) %>% 
             do({
               qts = round(quantile(.$val, c(0, 0.25, 0.5, 0.75, 1), na.rm = TRUE), 2)
               media = round(mean(.$val, na.rm = TRUE), 2)
               arquivo = ifelse(!is.null(interesse.path), interesse.name[[i]], files.nome[[i]])
               na = sum(is.na(.$val))
               data.frame(arquivo, t(qts), media, na)
             }), error = function(e) NULL))

# result.numericas = lapply(1:length(all.continuas), function(i)
#   tryCatch(all.continuas[[i]] %>%
#              select(everything()) %>%
#              gather('var', 'val') %>%
#              group_by(var) %>% 
#              do({
#                qts = round(quantile(.$val, c(0, 0.25, 0.5, 0.75, 1), na.rm = TRUE), 2)
#                media = round(mean(.$val, na.rm = TRUE), 2)
#                arquivo = arq_interesse.nome[[i]]
#                na = sum(is.na(.$val))
#                data.frame(arquivo, t(qts), media, na)
#              }), error = function(e) NULL))

# renomear a lista com os nomes dos arquivos disponibilizados
if (!is.null(interesse.path)) {
  names(result.numericas) = gsub("\\.csv$", "", interesse.name)
} else {
  names(result.numericas) = gsub("\\.csv$", "", files.nome)
}

# concatenando os resultados de cada data frame
numericas = bind_rows(result.numericas)
names(numericas) = c("Variavel", "Arquivo", "0%", "25%", "50%", "75%", "100%", "Media", "Faltante")

#####
##  CATEGÓRICAS
#####

# Faz um subset em cada data frame da lista separando apenas as variáveis que são categóricas
all.categoricas = lapply(1:length(all.csvs), function(i)
  tryCatch(select(all.csvs[[i]], which(sapply(all.csvs[[i]], is.character))), error = function(e) NULL))

# Faz os cálculos de interesse para as variáveis categóricas
result.categoricas = lapply(1:length(all.categoricas), function(i)
  tryCatch(gather(all.categoricas[[i]], "var", "value") %>%
             count(var, value) %>%
             mutate(prop = prop.table(n)) %>%
             mutate(arquivo = ifelse(!is.null(interesse.path), interesse.name[[i]], files.nome[[i]])),
           error = function(e) NULL))

# renomear a lista com os nomes dos arquivos disponibilizados
if (!is.null(interesse.path)) {
  names(result.categoricas) = gsub("\\.csv$", "", interesse.name)
} else {
  names(result.categoricas) = gsub("\\.csv$", "", files.nome)
}

# concatenando os resultados de cada data frame
categoricas = bind_rows(result.categoricas)
names(categoricas) = c("Variavel", "Fator", "N", "Proporcao", "Arquivo")

#####
##  DATAS
#####

# Faz um subset em cada data frame da lista separando apenas as variáveis que são datas
all.datas = lapply(1:length(all.csvs), function(i)
  tryCatch(select(all.csvs[[i]], which(sapply(all.csvs[[i]], is.Date))), error = function(e) NULL))

# Faz os cálculos de interesse para as variáveis datas
result.datas = lapply(1:length(all.datas), function(i)
  tryCatch(gather(all.datas[[i]], "var", "value") %>%
             select(everything()) %>%
             gather('var', 'val') %>%
             group_by(var) %>% 
             do({
               min = min(.$val)
               max = max(.$val)
               arquivo = ifelse(!is.null(interesse.path), interesse.name[[i]], files.nome[[i]])
               na = sum(is.na(.$val))
               data.frame(arquivo, min, max, na)
             }), error = function(e) NULL))

# renomear a lista com os nomes dos arquivos disponibilizados
if (!is.null(interesse.path)) {
  names(result.datas) = gsub("\\.csv$", "", interesse.name)
} else {
  names(result.datas) = gsub("\\.csv$", "", files.nome)
}

# concatenando os resultados de cada data frame
datas = bind_rows(result.datas)
names(datas) = c("Variavel", "Arquivo", "Ano mais antigo", "Ano mais recente", "Faltante")

#########################################
########  EXPORTANDO RESULTADOS  ########
#########################################

write_csv(dimensoes, "C:/Users/Natalia/integracao_novos_membros-wiki/code/R/Script_Univariada_Exemplo/Resultados/dimensao.csv" )
write_csv(numericas, "C:/Users/Natalia/integracao_novos_membros-wiki/code/R/Script_Univariada_Exemplo/Resultados/numericas.csv")
write_csv(categoricas, "C:/Users/Natalia/integracao_novos_membros-wiki/code/R/Script_Univariada_Exemplo/Resultados/categoricas.csv")
write_csv(datas, "C:/Users/Natalia/integracao_novos_membros-wiki/code/R/Script_Univariada_Exemplo/Resultados/datas.csv")
