##############################################
#####   ANÁLISE DESCRITIVA BIVARIADA     #####
##############################################

require(Hmisc)
require(plotly)
require(DT)
require(knitr)
require(vcd)
require(plyr)

# Os dados considerados pertencem a exemplos do software R. Para contemplar todos os tipos de
# análise não vamos "fixar" uma variável como sendo a resposta. Entretanto, no script a ser
# automatizado é interessante que o analista possa especificar qual sua variável dependente 
# (y) e quais as variáveis independentes (x).

dados = mtcars 

# Como imagino que esse script de análise bivariada será continuação do relatório de análise
# univariada, a declaração dos cabeçalhos e tipos de variáveis não se torna necessária. A se-
# guir serão descritas as 3 possibilidades de análise bivariada (dependendo do tipo de variá-
# vel que você estiver trabalhando). Para cada uma delas, será detalhado o tipo de saída es-
# perada.

######################################
#####  2 VARIÁVEIS QUANTITATIVAS  ####
######################################

# Obs.: aqui estamos considerando que uma das variáveis é fixa e contínua. Então, faremos
# todas as combinações possíveis entre a variável dependente e cada uma das variáveis indepen-
# tes, também quantitativas.

########
##### mpg e disp
########

##### Testes

t.test(dados$mpg, dados$disp)     # Teste t-student para duas médias
t.test(dados$mpg, dados$disp)[3]  # p-valor do teste
rcorr(cbind(dados$mpg, dados$disp), type=c("pearson")) # Correlação de Pearson
rcorr(cbind(dados$mpg, dados$disp), type=c("spearman")) # Correlação de Spearman

# Obs.: para saber qual tipo de correlação deve ser utilizado, é necessário fazer um teste
# de normalidade. Para os casos normais, o mais adequado é utilizar a correlação de Pearson,
# para os demais utiliza-se Spearman. O teste de normalidade é feito por: 
# ks.test(variavel, "pnorm"). Mais detalhes estão na wiki de análise exploratória

##### Gráficos

plot_ly(x = dados$mpg, y = dados$disp, mode = 'markers', showlegend = T) %>%
  layout(title = 'Gráfico de dispersão', xaxis = list(title = "mpg"), 
         yaxis = list(title = "disp")) 

# Da mesma forma que para os testes, para cada combinação de variável resposta e variáveis
# independentes, deve ser apresentado um gráfico de dispersão.

##### Regressão linear simples

reg = lm(dados$mpg ~ dados$disp) # Ajuste como ideia de pré-seleção de covariáveis
summary(reg) # Resultado do ajuste
reg.stdres = rstandard(reg) # Resíduo padronizado
ks.test(rstandard(reg), "pnorm") # Teste normalidade resíduos
ks.test(rstandard(reg), "pnorm")[2]  # p-valor do teste
qqnorm(reg.stdres, ylab="Standardized Residuals", xlab="Normal Scores", main="QQ plot dos resíduos padronizados") # QQ Plot
qqline(reg.stdres) # Adiciona linha QQ Plot
par(mfrow = c(2,2))
plot(reg)

# A saída desses testes pode ser exibida em uma única tabela, com 9 colunas, que exiba apenas
# os casos que foram significativos. Para esses, a tabela exibe: variável 1, variável 2, 
# valor-p do teste t, média da variável 1, média da variável 2, valor da correlação, valor-p
# da correlação, tipo de correlação (Pearson ou Spearman) e valor-p da regressão linear 
# simples.

#############################
#####   2 QUALITATIVAS   ####
#############################

# Obs.: aqui estamos considerando que uma das variáveis é fixa e qualitativa. Então, faremos
# todas as combinações possíveis entre a variável dependente e cada uma das variáveis indepen-
# tes, também qualitativas.

########
##### am e cyl
########

#### Medidas resumo

table(dados$cyl,dados$am) # Tabela de dupla entrada
tab2 = datatable(cbind(table(dados$cyl,dados$am)[,1], table(dados$cyl,dados$am)[,2]), colnames = c("automático", "manual")) # Tabela de dupla entrada para tabelas com mais de 10 linhas
kable(table(dados$cyl,dados$am), col.names = c("automático", "manual")) # Tabela de dupla entrada para tabelas com menos de 10 linhas)

# Essa parte talvez não seja interessante de ser apresentada. Apenas os testes já são um "bom
# começo". Isso pode ficar como algo complementar, a ser verificada a necessidade.

##### Gráficos

tabela3 = table(dados$cyl,dados$am)
grafcol = plot_ly(y = tabela3[,1], x = levels(dados$cyl), type = "bar", name = levels(dados$am)[1], showlegend = T) %>% # Gráfico de colunas
  add_trace(y = tabela3[,2], x = levels(dados$cyl), type = "bar", name = levels(dados$am)[2], showlegend = T)%>% 
  layout(title = "Gráfico de colunas - cylxam", xaxis = list(title = "cyl"), yaxis = list(title = "freq")) # Layout gráfico de colunas

tabela4 = table(dados$cyl,dados$am)
plot_ly(y = tabela4[,1], x = levels(dados$cyl), type = "bar", name = levels(dados$am)[1], showlegend = T) %>% # Gráfico de colunas tipo stack
  add_trace(y = tabela4[,2], x = levels(dados$cyl), type = "bar", name = levels(dados$am)[2], showlegend = T)%>%
  layout(title = "Cilindro", xaxis = list(title = "cyl"), yaxis = list(title = "freq"), barmode = "stack") # Layout gráfico de colunas # Se a variável utilizada apresentar valores negativos, no caso var. quanti, colocar barmode = 'relative'.

# Da mesma forma que para os testes, para cada combinação de variável resposta e variáveis
# independentes, deve ser apresentado um gráfico de barras. Aqui são apresentadas duas opções
# de gráfico de barras, qualquer uma delas pode ser utilizada.

##### Testes

rcorr(cbind(dados$cyl,dados$am), type=c("spearman")) # Correlação de Spearman
chisq.test(dados$cyl,dados$am) # Teste qui-quadrado

# A saída desses testes pode ser exibida em uma única tabela, com 5 colunas, que exiba apenas
# os casos que foram significativos. Para esses, a tabela exibe: variável 1, variável 2, 
# valor da correlação, valor-p da correlação, valor-p do teste qui-quadrado.

#############################
#####   QUANT E QUAL     ####
#############################

# Obs.: aqui estamos considerando que uma das variáveis é fixa (qualitativa ou quantitativa).
# Então, faremos todas as combinações possíveis entre a variável dependente e cada uma das 
# variáveis indepentes, também qualitativas.

########
##### am e mpg
########

#### Medidas resumo

ddply(dados, .(am), summarise, mean = round(mean(mpg),2), sd = round(sd(mpg),2)) # Função ddply como forma de apresentar medidas resumo para uma variável qualitativa

# Essa parte talvez não seja interessante de ser apresentada. Apenas os testes já são um "bom
# começo". Isso pode ficar como algo complementar, a ser verificada a necessidade.

#### Gráficos

# Boxplot por estratos

boxplot_estratos = plot_ly(y = dados$mpg, x = dados$am, type = "box", showlegend = TRUE) %>%
  layout(boxplot_estratos, yaxis = list(title = "mpg"), title = "Boxplots amxmpg", 
         xaxis = list(title = "am"))
boxplot_estratos

# Da mesma forma que para os testes, para cada combinação de variável resposta e variáveis
# independentes, deve ser apresentado um boxplot por estratos. 

#### Testes

# Nesse caso, há três tipos de análise, sendo que a escolha de qual teste utilizar depende
# da verificação do pressuposto de normalidade. Caso a distribuição não seja normal e a
# variável qualitativa tenha apenas dois níveis, utiliza-se o teste U de Mann-Whitney.
# Já se a variável tiver mais de dois níveis, utiliza-se o teste de Kruskal Wallis.
# Se a distribuição for normal, utiliza-se a Anova.

wilcox.test(dados$mpg ~ dados$am) # Teste U de Mann-Whitney
#wilcox.test(dados$medidas_grupo1, dados$medidas_grupo2, paired = TRUE) # Teste de Wilcoxon para amostras pareadas

kruskal.test(dados$mpg ~ dados$am) # Ajuste

aov(dados$mpg ~ dados$am) # Ajuste
summary(aov(dados$mpg ~ dados$am)) # Resultados do ajuste


# A saída desses testes pode ser exibida em uma única tabela, com 4 colunas, que exiba apenas
# os casos que foram significativos. Para esses, a tabela exibe: variável 1, variável 2, 
# valor-p do teste utilizado, teste utilizado. Mais detalhes sobre cada um desses testes 
# encontra-se na wiki de análise exploratória. 